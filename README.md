# gitlab-card

**gitlab-card** is a tool that generates SVG images containing GitLab project information, inspired by the ["gh-card" project](https://github.com/nwtgck/gh-card).  
This tool allows you to craft visually appealing SVG badges that display project details fetched from GitLab.

![Example gitlab-card](./docs/gyazo-uploader.svg)

You can generate SVG images easily on [https://gitlab-card.pages.dev/](https://gitlab-card.pages.dev/).

## Quick Start

### With Web UI

Go to [https://gitlab-card.pages.dev/](https://gitlab-card.pages.dev/) and enter the name of the project that will generate the image in the input field.

![frontend](./docs/frontend.png)

### Without Web UI

To generate an SVG image with GitLab project information, access the following URL format:

```
https://gitlab-card.bridgey.workers.dev/projects/:owner/:project
```

Replace `:owner` and `:project` with the appropriate values.

You can also use optional query parameters:

- `fullname`: Specify whether the project name should include the owner's username.
- `link_target`: Specify the target for the project link.

**Please note that the availability of the service at https://gitlab-card.bridgey.workers.dev is not guaranteed and it might be discontinued without prior notice.**

#### Examples

##### Using curl

```bash
curl https://gitlab-card.bridgey.workers.dev/projects/bridgey-public/gyazo-uploader > test.svg
```

##### In markdown

```markdown
[![bridgey-public/gyazo-uploader](https://gitlab-card.bridgey.workers.dev/projects/bridgey-public/gyazo-uploader?fullname=&link_target=_blank)](https://gitlab.com/bridgey-public/gyazo-uploader)
```

## Self-host on Cloudflare

### Backend

To use backend of gitlab-card, you need to have wrangler and Cloudflare account. After setting these up, follow these steps:

1. Create your project by clicking on `fork` button and clone into your local host.

2. Execute the following commands.

   ```shell
   cd backend

   npx wrangler dev

   # deploy your worker globally to Cloudflare Workers.
   npx wrangler publish
   ```

3. Configure the required secrets.

   Run the following command:

   ```bash
   npx wrangler put <secret>
   ```

   Secrets:

   - `TOKEN`: The access token of gitlab.com.

### Frontend

This section explains how to deploy to cloudflare pages.

1. Enter the url of backend to `.env`.

   ```shell
   cd frontend
   echo "VITE_IMAGE_SERVER_URL=<url of your backend>" >> .env
   ```

2. Install dependencies.

   ```shell
   npm install
   ```

3. Execute deploy command.

   ```shell
   npm run deploy
   ```

## License

This project is licensed under the [MIT License](LICENSE).

## Credits

gitlab-card is inspired by the [gh-card project](https://github.com/nwtgck/gh-card).

---

_Disclaimer: gitlab-card is not affiliated with GitLab Inc._
