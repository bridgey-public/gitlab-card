use std::collections::HashMap;
use tracing_subscriber::fmt::format::Pretty;
use tracing_subscriber::fmt::time::UtcTime;
use tracing_subscriber::prelude::*;
use tracing_web::{performance_layer, MakeConsoleWriter};
use worker::*;

mod gitlab;
use gitlab::get_project;
use gitlab::project;

mod svg_generator;
use svg_generator::generate_svg;

#[event(start)]
fn start() {
    let fmt_layer = tracing_subscriber::fmt::layer()
        .json()
        .with_ansi(false) // Only partially supported across JavaScript runtimes
        .with_timer(UtcTime::rfc_3339()) // std::time is not available in browsers
        .with_writer(MakeConsoleWriter); // write events to the console
    let perf_layer = performance_layer().with_details_from_fields(Pretty::default());
    tracing_subscriber::registry()
        .with(fmt_layer)
        .with(perf_layer)
        .init();
}

#[event(fetch)]
async fn main(req: Request, env: Env, _ctx: Context) -> Result<Response> {
    tracing::info!(request=?req, "Handling request");

    let router = Router::new();

    router
        .get_async("/", |_, _| async move {
            Response::ok("e.g. /projects/gitlab-org/gitlab-foss")
        })
        .get_async("/projects/:owner/:project", |req, ctx| async move {
            let owner = match ctx.param("owner") {
                Some(value) => value,
                None => return Response::error("invalid request", 400),
            };
            let proj = match ctx.param("project") {
                Some(value) => value,
                None => return Response::error("invalid request", 400),
            };

            let url = req.url().unwrap();
            let params = get_query_params(url)?;
            let is_fullname = match params.get("fullname") {
                Some(_) => true,
                None => false,
            };
            let link_target = match params.get("link_target") {
                Some(v) => v,
                None => "",
            };

            let project_name = format!("{}/{}", owner, proj);

            // Fetch project data from cache or GitLab API
            let token = ctx.secret("TOKEN")?.to_string();

            let kv = ctx.kv("GITLAB_CARD")?;
            let cache = kv.get(&project_name).text().await?;
            let result: project::ProjectProject = match cache {
                Some(v) => {
                    tracing::info!(project=?v, "get project data from cache");
                    serde_json::from_str(&v)?
                }
                None => match get_project(&token, &project_name).await {
                    Ok(value) => {
                        if let Some(v) = value {
                            tracing::info!(response=?v, "get project data");
                            let project_cache = serde_json::to_string(&v)?;
                            // cache with 20 minutes expiration
                            kv.put(&project_name, project_cache)?
                                .expiration_ttl(60 * 20)
                                .execute()
                                .await?;
                            v
                        } else {
                            // Return an error response for unsuccessful project retrieval
                            return Response::error("Server error", 500);
                        }
                    }
                    // Handle API error
                    Err(e) => return Response::error(e.to_string(), 500),
                },
            };

            // Extract information for generating svg
            let empty = "".to_string();
            let (language, color) = {
                if let Some(langs) = &result.languages {
                    match langs.first() {
                        Some(first_language) => (
                            Some(first_language.name.as_str()),
                            Some(first_language.color.as_ref().unwrap_or(&empty).as_str()),
                        ),
                        None => (Some(""), Some("")),
                    }
                } else {
                    (Some(""), Some(""))
                }
            };
            let binding = result.description.unwrap_or("".to_string());
            let desc = binding.as_str();
            let n_stars = result.star_count;
            let n_forks = result.forks_count;

            let is_fork = match result.fork_details {
                Some(_) => true,
                None => false,
            };

            // Generate SVG with the extracted project information
            let svg_data = generate_svg(
                owner,
                proj,
                is_fullname,
                link_target,
                language,
                desc,
                n_stars.try_into().unwrap(),
                n_forks.try_into().unwrap(),
                is_fork,
                color,
            );

            let mut header = Headers::new();
            let _ = header.set("Content-Type", "image/svg+xml");
            Ok(Response::ok(svg_data.svg).unwrap().with_headers(header))
        })
        .run(req, env)
        .await
}

// extract query parameters from the URL
fn get_query_params(url: Url) -> Result<HashMap<String, String>> {
    Ok(url.query_pairs().into_owned().collect())
}
