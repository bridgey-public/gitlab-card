use anyhow::Result;
use graphql_client::reqwest::post_graphql;
use graphql_client::GraphQLQuery;
use reqwest::Client;

type Color = String;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/schema.json",
    query_path = "src/query.gql",
    variables_derives = "Debug",
    response_derives = "Debug,Deserialize,Serialize"
)]
pub struct Project;

pub async fn get_project(
    token: &str,
    full_project_name: &str,
) -> Result<Option<project::ProjectProject>> {
    let variables = project::Variables {
        project_path: full_project_name.to_string(),
    };

    let client = Client::builder()
        .default_headers(
            std::iter::once((
                reqwest::header::AUTHORIZATION,
                reqwest::header::HeaderValue::from_str(&format!("Bearer {}", token.to_string()))
                    .unwrap(),
            ))
            .collect(),
        )
        .build()?;

    let response_body =
        post_graphql::<Project, _>(&client, "https://gitlab.com/api/graphql", variables).await?;
    tracing::info!(response=?response_body, "Get Response");

    let response_data: project::ResponseData = response_body.data.expect("missing response data");

    Ok(response_data.project)
}
