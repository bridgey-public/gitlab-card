use svg::node::element::{Anchor, Circle, Definitions, Group, Path, Rectangle, Text};
use svg::Document;

pub struct SvgData {
    pub width: u32,
    pub height: u32,
    pub svg: String,
}

fn description_to_lines(description: &str) -> Vec<String> {
    const SPLITTER: &str = " ";
    let words: Vec<&str> = description.split(SPLITTER).collect();
    let mut line = String::new();
    let mut lines = Vec::new();

    for word in words.iter() {
        line.push_str(word);
        line.push_str(SPLITTER);

        if line.len() > 55 {
            lines.push(line.clone());
            line.clear();
        }
    }

    if !line.is_empty() {
        lines.push(line);
    }

    lines
}

fn gitlab_number_string(n: u32) -> String {
    if n < 950 {
        n.to_string()
    } else {
        let k = n as f32 / 1000.0;

        if k < 99.9 {
            format!("{:.1}k", k)
        } else {
            format!("{}k", k.round())
        }
    }
}

fn get_gitlab_project_url(owner_name: &str, short_project_name: &str) -> String {
    format!("https://gitlab.com/{}/{}", owner_name, short_project_name)
}

fn get_gitlab_starrers_url(owner_name: &str, short_project_name: &str) -> String {
    format!(
        "https://gitlab.com/{}/-/starrers",
        get_gitlab_project_url(owner_name, short_project_name)
    )
}

// gitlab has not have a url like github 'network/members'?
fn get_gitlab_forks_url(owner_name: &str, short_project_name: &str) -> String {
    format!(
        "https://gitlab.com/{}/-/forks",
        get_gitlab_project_url(owner_name, short_project_name)
    )
}

pub fn generate_svg(
    owner: &str,
    project: &str,
    is_fullname: bool,
    link_target: &str,
    language: Option<&str>,
    description: &str,
    n_stars: u32,
    n_forks: u32,
    is_fork: bool,
    color: Option<&str>,
) -> SvgData {
    let project_name_in_image = if is_fullname {
        format!("{}/{}", owner, project)
    } else {
        project.to_string()
    };

    // Convert description to lines
    let description_lines = description_to_lines(description);

    // Create description element and last y coordinate
    let mut y = 65;
    let diff = 21;
    let mut description_elems = Group::new()
        .set("fill", "#586069")
        .set("fill-opacity", "1")
        .set("stroke", "#586069")
        .set("stroke-opacity", "1")
        .set("stroke-width", "1")
        .set("stroke-linecap", "square")
        .set("stroke-linejoin", "bevel")
        .set("transform", format!("matrix(1,0,0,1,0,0)"));
    for line in &description_lines {
        let text_elem = Text::new("")
            .set("fill", "#586069")
            .set("fill-opacity", "1")
            .set("stroke", "none")
            .set("xml:space", "preserve")
            .set("x", "17")
            .set("y", y)
            .set("font-family", "sans-serif")
            .set("font-size", "14")
            .set("font-weight", "400")
            .set("font-style", "normal")
            .add(svg::node::Text::new(line));

        description_elems = description_elems.add(text_elem);
        y += diff;
    }

    let last_description_y = y - diff;

    let width = 442;
    let height = last_description_y + 43;
    let language_next_x = match language {
        Some(lang) => 60 + (5 * lang.len()),
        None => 16,
    };

    let star_icon = Anchor::new()
        .set("target", link_target)
        .set("href", get_gitlab_starrers_url(owner, project))
        .add(
            Path::new()
                .set("vector-effect", "none")
                .set("fill-rule", "evenodd")
                .set(
                    "d",
                    "M14,6 L9.1,5.36 L7,1 L4.9,5.36 L0,6 L3.6,9.26 L2.67,14 L7,11.67 L11.33,14 L10.4,9.26 L14,6",
                ),
        );

    let fork_icon = Anchor::new()
        .set("target", link_target)
        .set("href", get_gitlab_forks_url(owner, project))
        .add(
            Path::new()
                .set("vector-effect", "none")
                .set("fill-rule", "evenodd")
                .set(
                    "d",
                    "M10,5 C10,3.89 9.11,3 8,3 C7.0966,2.99761 6.30459,3.60318 6.07006,4.47561 C5.83554,5.34804 6.21717,6.2691 7,6.72 L7,7.02 C6.98,7.54 6.77,8 6.37,8.4 C5.97,8.8 5.51,9.01 4.99,9.03 C4.16,9.05 3.51,9.19 2.99,9.48 L2.99,4.72 C3.77283,4.2691 4.15446,3.34804 3.91994,2.47561 C3.68541,1.60318 2.8934,0.997613 1.99,1 C0.88,1 0,1.89 0,3 C0.00428689,3.71022 0.384911,4.3649 1,4.72 L1,11.28 C0.41,11.63 0,12.27 0,13 C0,14.11 0.89,15 2,15 C3.11,15 4,14.11 4,13 C4,12.47 3.8,12 3.47,11.64 C3.56,11.58 3.95,11.23 4.06,11.17 C4.31,11.06 4.62,11 5,11 C6.05,10.95 6.95,10.55 7.75,9.75 C8.55,8.95 8.95,7.77 9,6.73 L8.98,6.73 C9.59,6.37 10,5.73 10,5 M2,1.8 C2.66,1.8 3.2,2.35 3.2,3 3.2,3.65 2.65,4.2 2,4.2 C1.35,4.2 0.8,3.65 0.8,3 0.8,2.35 1.35,1.8 2,1.8 M2,14.21 C1.34,14.21 0.8,13.66 0.8,13.01 C0.8,12.36 1.35,11.81 2,11.81 C2.65,11.81 3.2,12.36 3.2,13.01 C3.2,13.66 2.65,14.21 2,14.21 M8,6.21 C7.34,6.21 6.8,5.66 6.8,5.01 C6.8,4.36 7.35,3.81 8,3.81 C8.65,3.81 9.2,4.36 9.2,5.01 C9.2,5.66 8.65,6.21 8,6.21 ",
                ),
        );

    let url_and_number_str: Option<(String, String)> = {
        if n_stars > 0 {
            Some((
                get_gitlab_starrers_url(owner, project),
                gitlab_number_string(n_stars),
            ))
        } else if n_forks > 0 {
            Some((
                get_gitlab_forks_url(owner, project),
                gitlab_number_string(n_forks),
            ))
        } else {
            None
        }
    };

    // Build the SVG document
    let mut svg_content = Document::new()
        .set("xmlns", "http://www.w3.org/2000/svg")
        .set("xmlns:xlink", "http://www.w3.org/1999/xlink")
        .set("width", width)
        .set("height", height + 1)
        .set("version", "1.2")
        .set("baseProfile", "tiny");

    svg_content = svg_content.add(Definitions::new());

    let mut group = Group::new()
        .set("fill", "none")
        .set("stroke", "black")
        .set("stroke-width", "1")
        .set("fill-rule", "evenodd")
        .set("stroke-linecap", "square")
        .set("stroke-linejoin", "bevel");

    group = group.add(
        Group::new()
            .set("fill", "#ffffff")
            .set("fill-opacity", "1")
            .set("stroke", "none")
            .set("transform", "matrix(1,0,0,1,0,0)")
            .add(
                Rectangle::new()
                    .set("x", 0)
                    .set("y", 0)
                    .set("width", 440)
                    .set("height", height + 1),
            ),
    );

    // Add rectangle for boarder
    group = group.add(
        Rectangle::new()
            .set("x", 0)
            .set("y", 0)
            .set("width", 441)
            .set("height", height)
            .set("stroke", "#eaecef")
            .set("stroke-width", 2),
    );

    // Add fork or project icon
    let icon_path = if is_fork {
        "M8 1a1.993 1.993 0 00-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 002 1a1.993 1.993 0 00-1 3.72V6.5l3 3v1.78A1.993 1.993 0 005 15a1.993 1.993 0 001-3.72V9.5l3-3V4.72A1.993 1.993 0 008 1zM2 4.2C1.34 4.2 .8 3.65 .8 3c0-.65 .55-1.2 1.2-1.2 .65 0 1.2 .55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65 .55-1.2 1.2-1.2 .65 0 1.2 .55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65 .55-1.2 1.2-1.2 .65 0 1.2 .55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"
    } else {
        "M4,9 L3,9 L3,8 L4,8 L4,9 M4,6 L3,6 L3,7 L4,7 L4,6 M4,4 L3,4 L3,5 L4,5 L4,4 M4,2 L3,2 L3,3 L4,3 L4,2 M12,1 L12,13 C12,13.55 11.55,14 11,14 L6,14 L6,16 L4.5,14.5 L3,16 L3,14 L1,14 C0.45,14 0,13.55 0,13 L0,1 C0,0.45 0.45,0 1,0 L11,0 C11.55,0 12,0.45 12,1 M11,11 L1,11 L1,13 L3,13 L3,12 L6,12 L6,13 L11,13 L11,11 M11,1 L2,1 L2,10 L11,10 L11,1"
    };

    let icon = Group::new()
        .set("fill", "#586069")
        .set("fill-opacity", "1")
        .set("stroke", "none")
        .set("transform", "matrix(1.25,0,0,1.25,17,21)")
        .add(Path::new().set("d", icon_path));

    group = group.add(icon);

    // Add project name
    let project_name = Group::new()
        .set("fill", "#0366d6")
        .set("fill-opacity", "1")
        .set("stroke", "#0366d6")
        .set("stroke-opacity", "1")
        .set("stroke-width", "1")
        .set("stroke-linecap", "square")
        .set("stroke-linejoin", "bevel")
        .set("transform", "matrix(1,0,0,1,0,0)")
        .add(
            Anchor::new()
                .set("target", link_target)
                .set("href", get_gitlab_project_url(owner, project))
                .add(
                    Text::new("")
                        .set("fill", "#0366d6")
                        .set("fill-opacity", "1")
                        .set("stroke", "none")
                        .set("xml:space", "preserve")
                        .set("x", 41)
                        .set("y", 33)
                        .set("font-family", "sans-serif")
                        .set("font-size", 16)
                        .set("font-weight", 630)
                        .set("font-style", "normal")
                        .add(svg::node::Text::new(project_name_in_image)),
                ),
        );

    group = group.add(project_name);

    // Description
    group = group.add(description_elems);

    // Language color
    if let Some(color) = color {
        let language_color_circle = Circle::new()
            .set("cx", 23)
            .set("cy", format!("{}", last_description_y + 21))
            .set("r", 7)
            .set("stroke", "none")
            .set("fill", color);

        let language_color_group = Group::new().add(language_color_circle);

        group = group.add(language_color_group);
    }
    let language_elem = Group::new()
        .set("fill", "#24292e")
        .set("fill-opacity", "1")
        .set("stroke", "#24292e")
        .set("stroke-opacity", "1")
        .set("stroke-width", "1")
        .set("stroke-linecap", "square")
        .set("stroke-linejoin", "bevel")
        .set("transform", "matrix(1,0,0,1,0,0)")
        .add(
            Text::new("")
                .set("fill", "#24292e")
                .set("fill-opacity", "1")
                .set("stroke", "none")
                .set("xml:space", "preserve")
                .set("x", 33)
                .set("y", last_description_y + 26)
                .set("font-family", "sans-serif")
                .set("font-size", 12)
                .set("font-weight", 400)
                .set("font-style", "normal")
                .add(svg::node::Text::new(language.unwrap_or("").to_string())),
        );
    group = group.add(language_elem);

    // Star or Fork icon or none
    let star_or_fork_icon = if n_stars > 0 {
        Some(star_icon)
    } else if n_forks > 0 {
        Some(fork_icon.clone())
    } else {
        None
    };

    if let Some(star_or_fork_icon) = star_or_fork_icon {
        let star_or_fork_icon_group = Group::new()
            .set("fill", "#000000")
            .set("fill-opacity", "1")
            .set("stroke", "none")
            .set(
                "transform",
                format!(
                    "matrix(1,0,0,1,{},{})",
                    language_next_x,
                    last_description_y + 13
                ),
            )
            .add(star_or_fork_icon);

        group = group.add(star_or_fork_icon_group);
    }

    // The number of stars or fork
    if let Some((url, number_str)) = url_and_number_str {
        let stars_or_forks_number = Anchor::new()
            .set("target", link_target)
            .set("href", url)
            .add(
                Text::new("")
                    .add(svg::node::Text::new(number_str))
                    .set("fill", "#586069")
                    .set("fill-opacity", "1")
                    .set("stroke", "none")
                    .set("xml:space", "preserve")
                    .set("x", language_next_x + 21)
                    .set("y", last_description_y + 26)
                    .set("font-family", "sans-serif")
                    .set("font-size", 12)
                    .set("font-weight", 400)
                    .set("font-style", "normal"),
            );

        let stars_or_forks_number_group = Group::new()
            .set("fill", "#586069")
            .set("fill-opacity", "1")
            .set("stroke", "#586069")
            .set("stroke-opacity", "1")
            .set("stroke-width", "1")
            .set("stroke-linecap", "square")
            .set("stroke-linejoin", "bevel")
            .set("transform", "matrix(1,0,0,1,0,0)")
            .add(stars_or_forks_number);

        group = group.add(stars_or_forks_number_group);
    }

    // Fork icon or none
    if n_stars > 0 && n_forks > 0 {
        let fork_icon_group = Group::new()
            .set("fill", "#000000")
            .set("fill-opacity", "1")
            .set("stroke", "none")
            .set(
                "transform",
                format!(
                    "matrix(1,0,0,1,{},{})",
                    language_next_x + 63,
                    last_description_y + 13
                ),
            )
            .add(fork_icon);

        group = group.add(fork_icon_group);

        // The number of forks or none
        let forks_number_group = Group::new()
            .set("fill", "#586069")
            .set("fill-opacity", "1")
            .set("stroke", "#586069")
            .set("stroke-opacity", "1")
            .set("stroke-width", "1")
            .set("stroke-linecap", "square")
            .set("stroke-linejoin", "bevel")
            .set("transform", "matrix(1,0,0,1,0,0)")
            .add(
                Anchor::new()
                    .set("target", link_target)
                    .set("href", get_gitlab_forks_url(owner, project))
                    .add(
                        Text::new("")
                            .add(svg::node::Text::new(gitlab_number_string(n_forks)))
                            .set("fill", "")
                            .set("fill-opacity", "1")
                            .set("stroke", "none")
                            .set("xml:space", "preserve")
                            .set("font-family", "sans-serif")
                            .set("font-size", 12)
                            .set("font-weight", 400)
                            .set("font-style", "normal")
                            .set(
                                "transform",
                                format!(
                                    "matrix(1,0,0,1,{},{})",
                                    language_next_x + 80,
                                    last_description_y + 26
                                ),
                            ),
                    ),
            );

        group = group.add(forks_number_group);
    }

    // Add the complete group to the SVG content
    svg_content = svg_content.add(group);

    SvgData {
        width,
        height,
        svg: svg_content.to_string(),
    }
}
