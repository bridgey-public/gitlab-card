#!/usr/bin/env bash

type -p rustup >/dev/null || (curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain stable)
source $HOME/.cargo/env

# pnpm
curl -fsSL https://get.pnpm.io/install.sh | sh -
cat <<EOF >>$HOME/.bashrc
export PNPM_HOME="/home/vscode/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac

To start using pnpm, run:
source /home/vscode/.bashrc
EOF
pnpm install-completion bash

type -p fish >/dev/null && (set -Ua fish_user_paths "$HOME/.local/share/pnpm" && pnpm install-completion fish)
