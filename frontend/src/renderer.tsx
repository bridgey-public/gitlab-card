import "hono";
import { jsxRenderer } from "hono/jsx-renderer";
import { html, raw } from "hono/html";

declare module "hono" {
  interface ContextRenderer {
    (content: string, props?: { title?: string }): Response;
  }
}

export const renderer = jsxRenderer(
  ({ children, title }) => {
    return html` <html>
      <head>
        ${import.meta.env.PROD
        ? raw('<script type="module" src="/static/client.js"></script>')
        : raw('<script type="module" src="/src/client.ts"></script>')
      }
        <title>${title}</title>
      </head>
      <body>
        ${children}
      </body>
      <footer class="p-1 mt-auto text-sm text-center text-gray-500">
        <p>
          This site is similar to
          <a href="https://gh-card.dev" class="underline">gh-card.dev</a> but
          not related. I referenced that code and modified it for GitLab.
        </p>
      </footer>
    </html>`;
  },
  {
    docType: true,
  },
);
