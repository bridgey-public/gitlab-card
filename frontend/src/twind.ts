// ref: https://techblog.raksul.com/entry/2023/11/08/173335#Hono%E3%81%A7Twind%E3%82%92%E4%BD%BF%E3%81%86
//
import { createMiddleware } from "hono/factory";
import { inline, install } from "@twind/core";

// response bodyにTailwindを使うために必要なstyleタグを挿入する
// see: https://twind.style/packages/@twind/core#inline
export const tailwindStyleTagInjector = (config: any) => {
  install(config);

  return createMiddleware(async (c, next) => {
    await next();

    if (!c.res.body) {
      return;
    }

    const stream = c.res.body.pipeThrough(new TextDecoderStream());
    const buffer: string[] = [];

    for await (const chunk of stream) {
      buffer.push(chunk);
    }

    const html = buffer.join();
    const inserted_html = inline(html);

    c.res = new Response(inserted_html, c.res);
  });
};
