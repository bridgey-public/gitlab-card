import { AlpineComponents } from "@nxtlvlsoftware/alpine-typescript";
import { GitLabCardGenerator } from "./components/GitLabCardGenerator";

AlpineComponents.bootstrap({
  bootstrapAlpine: true,
  components: {
    generator: GitLabCardGenerator,
  },
  logErrors: true,
});
