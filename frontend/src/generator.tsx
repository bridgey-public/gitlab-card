import { html } from "hono/html";

export const Generator = () => {
  return html` <div x-data="generator" class="flex flex-col min-h-full">
    <h3 class="mt-2 text-xl font-bold text-center">Example</h3>
    <template x-for="example in exampleProjects">
      <span class="flex flex-col justify-center items-center">
        <a x-bind:href="getGitLabProjUrl(example)">
          <img x-bind:src="getImgUrl(example, true, '')" />
        </a>
      </span>
    </template>

    <hr class="my-4 border-black border-solid border-1" />
    <p class="flex flex-row gap-2 justify-center items-center text-center">
      <input
        type="text"
        x-model="projName"
        placeholder="user/project"
        class="py-1 my-1 text-xl text-center rounded-md border-gray-300 focus:ring-1 focus:outline-none border-1 focus:border-sky-500"
      />
      <button
        x-on:click="update()"
        class="py-1 px-4 my-1 text-xl font-semibold text-center text-white bg-blue-600 rounded-lg border border-transparent hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none"
      >
        Generate
      </button>
      <br />
    </p>

    <div
      x-cloak
      x-show="generated"
      class="flex flex-col gap-2 justify-center items-center my-4 mx-auto max-w-4xl"
    >
      <p class="flex flex-row gap-2 items-center text-center">
        <input type="checkbox" x-model="useFullName" />
        Full name
      </p>
      <template x-if="linkTarget === ''">
        <a x-bind:href="gitLabProjUrl">
          <img x-bind:src="imageUrl" />
        </a>
      </template>

      <template x-if="linkTarget !== ''">
        <object type="image/svg+xml" x-bind:data="imageUrl"></object>
      </template>

      <h3 class="mt-2 text-xl font-bold">Image URL</h3>
      <input
        type="text"
        x-bind:value="imageUrl"
        class="block w-full text-center border border-black focus:ring-1 focus:outline-none focus:border-sky-500"
      />
      <h3 class="mt-2 text-xl font-bold">HTML</h3>
      <p class="flex flex-row gap-2 items-center">
        Link target:
        <select
          x-model="linkTarget"
          class="text-sm text-center rounded focus:ring-1 focus:ring-blue-300 focus:outline-none"
        >
          <option value="">none</option>
          <option value="_top">_top</option>
          <option value="_parent">_parent</option>
          <option value="_blank">_blank</option>
        </select>
      </p>
      <textarea
        rows="3"
        x-text="embedHtml"
        class="block w-full text-center border border-black focus:ring-1 focus:outline-none focus:border-sky-500"
      ></textarea>
      <h3 class="mt-2 text-xl font-bold">Markdown</h3>
      <textarea
        x-text="embedMarkdown"
        rows="3"
        class="block w-full text-center border border-black focus:ring-1 focus:outline-none focus:border-sky-500"
      ></textarea>
      <h3 class="mt-2 text-xl font-bold">
        <a href="https://scrapbox.io" target="_blank">Scrapbox</a>
      </h3>
      <textarea
        x-text="embedScrapbox"
        rows="3"
        class="block w-full text-center border border-black focus:ring-1 focus:outline-none focus:border-sky-500"
      ></textarea>
    </div>
  </div>`;
};
