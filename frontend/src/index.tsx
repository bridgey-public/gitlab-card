import { Hono } from "hono";
import { renderer } from "./renderer";
import { Generator } from "./generator";

import { tailwindStyleTagInjector } from "./twind";
import presetTailwind from "@twind/preset-tailwind";

const app = new Hono();

app.use(
  "*",
  tailwindStyleTagInjector({
    presets: [presetTailwind()],
  }),
);

app.get("*", renderer);

app.get("/", (c) => {
  return c.render(
    <div id="app" class="mx-auto">
      <h1 class="py-8 px-4 mx-auto mt-4 font-sans text-4xl font-bold text-center">
        GitLab Project Card
        <br />
        for every web
      </h1>
      <a
        href="https://gitlab.com/bridgey-public/gitlab-card"
        target="_blank"
        rel="noreferrer"
        class="flex flex-row gap-1 justify-center"
      >
        <img src="/static/gitlab.svg" alt="GitLab Logo" style="width: 1em;" />{" "}
        <pre class="underline">GitLab</pre>
      </a>
      <Generator />
    </div>,
  );
});

export default app;
