import { AlpineComponent } from "@nxtlvlsoftware/alpine-typescript";

export const DefaultGeneratorComponentName = "generator";

type LinkTarget = "" | "_top" | "_parent" | "_blank";

export class GitLabCardGenerator extends AlpineComponent {
  constructor(
    private exampleProjects = ["gitlab-org/gitlab-foss"],

    private projName = "",
    private useFullName = false,
    private linkTarget: LinkTarget = "",

    private generated = false,
    private gitLabProjUrl = "",
    private imageUrl = "",
    private embedHtml = "",
    private embedMarkdown = "",
    private embedScrapbox = "",
  ) {
    super();
  }

  public init(): void {
    this.$watch("useFullName", () => {
      if (this.generated) {
        this.update();
      }
    });
    this.$watch("linkTarget", () => {
      if (this.generated) {
        this.update();
      }
    });
  }

  private cleanProjName(): string {
    const noSpaces = this.projName.replace(/\s/g, "");
    try {
      // Extract path part
      // e.g. "https://gitlab.com/gitlab-org/gitlab-foss" => "gitlab-org/gitlab-foss"
      // e.g. "https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/index.md?ref_type=heads" => "gitlab-org/gitlab-foss"
      const [user, name, ..._] = new URL(noSpaces).pathname.substring(1).split("/");
      return `${user}/${name}`;
    } catch {
      return noSpaces;
    }
  }

  private update(): void {
    this.gitLabProjUrl = this.getGitLabProjUrl(this.cleanProjName());
    this.imageUrl = this.getImgUrl(this.cleanProjName(), this.useFullName, this.linkTarget);
    this.embedHtml = this.getEmbedHtml(this.cleanProjName(), this.useFullName, this.linkTarget);
    this.embedMarkdown = this.getEmbedMarkdown(this.cleanProjName(), this.useFullName);
    this.embedScrapbox = this.getEmbedScrapbox(this.cleanProjName(), this.useFullName);
    this.generated = true;
  }

  private getGitLabProjUrl(projName: string): string {
    return `https://gitlab.com/${projName}`;
  }

  private getImgUrl(projName: string, useFullName: boolean, linkTarget: LinkTarget) {
    const url = new URL(`${import.meta.env.VITE_IMAGE_SERVER_URL}/projects/${projName}`);
    if (useFullName) {
      url.searchParams.set("fullname", "");
    }
    if (linkTarget) {
      url.searchParams.set("link_target", linkTarget);
    }
    return url.href;
  }

  private getEmbedHtml(projName: string, useFullName: boolean, linkTarget: LinkTarget): string {
    return linkTarget
      ? `<object type="image/svg+xml" data="${this.getImgUrl(
        projName,
        useFullName,
        linkTarget,
      )}"></object>`
      : `<a href="${this.getGitLabProjUrl(projName)}"><img src="${this.getImgUrl(
        projName,
        useFullName,
        linkTarget,
      )}"></a>`;
  }

  private getEmbedMarkdown(projName: string, useFullName: boolean): string {
    return `[![${projName} - GitLab](${this.getImgUrl(
      projName,
      useFullName,
      "",
    )})](${this.getGitLabProjUrl(projName)})`;
  }

  private getEmbedScrapbox(projName: string, useFullName: boolean): string {
    return `[${this.getImgUrl(projName, useFullName, "")} ${this.getGitLabProjUrl(projName)}]`;
  }
}
